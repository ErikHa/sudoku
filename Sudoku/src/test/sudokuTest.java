package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Sudoku.Sudoku;

public class sudokuTest {
	Sudoku game;
	int[][] testGrid;
	
	
	@Before
	public void setUp() throws Exception {
		game = new Sudoku();
		testGrid = new int[9][9];
		for(int i = 0; i < 9; i++) {
			for(int k = 0; k < 9; k++) {
				testGrid[i][k] = 0;
			}
		}
	}

	@After
	public void tearDown() throws Exception {
		game = null;
		testGrid = null;
	}

	
	@Test
	public void testFall1() {
		assertTrue("solve returns False but should be True", game.solve(testGrid));
	}
	
	
	@Test
	public void testFall2() {
		testGrid[0][0] = 5;
		testGrid[0][1] = 5;
		assertFalse("solve returns True but should be False", game.solve(testGrid));
	}
	
	
	@Test
	public void testFall3() {
		testGrid[0][0] = 1;
		testGrid[0][1] = 2;
		testGrid[0][2] = 3;
		testGrid[1][0] = 4;
		testGrid[1][1] = 5;
		testGrid[1][2] = 6;
		testGrid[2][3] = 7;
		assertFalse("solve returns True but should be False", game.solve(testGrid));
		
		testGrid[2][3] = 0;
		assertTrue("solve returns False but should be True", game.solve(testGrid));
	}
	
	
	@Test
	public void testFall4() {
		testGrid[0][0] = 5;
		testGrid[0][3] = 5;
		assertFalse("solve returns True but should be False", game.solve(testGrid));
		
		game.clear();
		
		assertTrue("solve returns False but should be True", game.solve(testGrid));
	}
	
	
	@Test
	public void testFall5() {
		testGrid[0][2] = 8;
		testGrid[0][5] = 9;
		testGrid[0][7] = 6;
		testGrid[0][8] = 2;
		
		testGrid[1][8] = 5;
		
		testGrid[2][0] = 1;
		testGrid[2][2] = 2;
		testGrid[2][3] = 5;
		
		testGrid[3][3] = 2;
		testGrid[3][4] = 1;
		testGrid[3][7] = 9;
		
		testGrid[4][1] = 5;
		testGrid[4][6] = 6;
		
		testGrid[5][0] = 6;
		testGrid[5][7] = 2;
		testGrid[5][8] = 8;
		
		testGrid[6][0] = 4;
		testGrid[6][1] = 1;
		testGrid[6][3] = 6;
		testGrid[6][5] = 8;
		
		testGrid[7][0] = 8;
		testGrid[7][1] = 6;
		testGrid[7][4] = 3;
		testGrid[7][6] = 1;
		
		testGrid[8][6] = 4;
		
		assertTrue("solve returns False but should be True", game.solve(testGrid));
	}
}
