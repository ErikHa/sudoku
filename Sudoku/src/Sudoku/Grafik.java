package Sudoku;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class Grafik extends Application {

	public void start(Stage primaryStage) throws Exception {
		BorderPane root = new BorderPane();
		GridPane rot = new GridPane();
		Scene scene = new Scene(root, 554, 553);
		primaryStage.setTitle("Sudoku");
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.setResizable(false);

		Sudoku game = new Sudoku();
		HBox bottomBox = new HBox();
		Button clear = new Button("Clear");
		Button solve = new Button("Solve");
		bottomBox.getChildren().addAll(clear, solve);
		rot.setHgap(2);
		rot.setVgap(2);
		rot.setStyle("-fx-background-color: #000000");

		// Skapar en 9x9 matris av textfält

		TextField[][] kart = new TextField[9][9];
		for (int i = 0; i < kart.length; i++) {
			for (int k = 0; k < kart.length; k++) {
				TextField testField = new TextField();
				testField.textProperty().addListener((unused_arg, oldValue, newValue) -> {
					if (newValue.length() > 1) {
						testField.setText(oldValue);
					}
					if (!newValue.equals("") && !Character.isDigit(newValue.charAt(0)) || newValue.equals("0")) {
						testField.setText(oldValue);
					}
				});
				kart[i][k] = testField;
				rot.addRow(i, kart[i][k]);
				testField.setPrefSize(root.getWidth() / 9, root.getWidth() / 9);
				kart[i][k].setFont(Font.font("Verdana", FontWeight.BOLD, 25));
				kart[i][k].setAlignment(Pos.CENTER);
			}
		}

		for (int i = 0; i < 9; i++) {
			for (int k = 0; k < 9; k++) {

			}
		}
		// Målar rutorna
		for (int i = 0; i < kart.length; i++) {
			for (int k = 0; k < kart.length; k++) {
				if ((i < 6 && i > 2) || (k < 6 && k > 2)) {
				} else {
					kart[i][k].setStyle("-fx-background-color: #FF8C00");
				}
				if ((i < 6 && i > 2) && (k < 6 && k > 2)) {
					kart[i][k].setStyle("-fx-background-color: #FF8C00");
				}
			}
		}

		// Knapp Event

		// Använder metoden clear i sudoku för att nollställa matrisen och tömmer alla
		// textfält
		clear.setOnAction(even -> {
			game.clear();
			for (int i = 0; i < 9; i++) {
				for (int k = 0; k < 9; k++) {
					kart[i][k].clear();
				}
			}
		});

		// Läser av alla textfält och skicka in en vektor med skrivna värdet till solve
		// Om testField är tomt skicka "0"
		solve.setOnAction(event -> {
			int[][] temp = new int[9][9];
			boolean valid = true;
			for (int i = 0; i < 9; i++) {
				for (int k = 0; k < 9; k++) {
					if (kart[i][k].getText().equals("1") || kart[i][k].getText().equals("2")
							|| kart[i][k].getText().equals("3") || kart[i][k].getText().equals("4")
							|| kart[i][k].getText().equals("5") || kart[i][k].getText().equals("6")
							|| kart[i][k].getText().equals("7") || kart[i][k].getText().equals("8")
							|| kart[i][k].getText().equals("9")) {
						temp[i][k] = Integer.parseInt(kart[i][k].getText()); // Sparar egen skrivna värden från
																				// Sudokufönstret i en vektor som är
																				// inparameter till solve (Behöver göra
																				// String till int)
					} else {
						temp[i][k] = 0;
					}
				}
			}

			if (valid) {
				boolean b = game.solve(temp);

				if (b) {
					temp = game.getSudoku();
					for (int i = 0; i < 9; i++) {
						for (int k = 0; k < 9; k++) {
							kart[i][k].setText("" + temp[i][k]);
						}
					}
				} else {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("WARNING");
					alert.setHeaderText("No solution");
					alert.setContentText("Try again!");
					alert.showAndWait();
				}
			}
		});
		// Slut Knapp Event

		root.setCenter(rot);
		root.setBottom(bottomBox);

	}

	public static void main(String[] args) {
		Application.launch(args);
	}

}
