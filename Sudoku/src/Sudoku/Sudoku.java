package Sudoku;

/**
 * 
 * @author Erik Hansson and Nils Cederberg
 *
 */
public class Sudoku {
	private int[][] sudoku;

	/**
	 * This program is for solving a sudoku.
	 */
	public Sudoku() {
		sudoku = new int[9][9];
	}

	/**
	 * Returns the sudoku.
	 * 
	 * @return the matrix that represents the sudoku
	 */
	public int[][] getSudoku() {
		int[][] returnsudoku = sudoku.clone();
		return returnsudoku;
	}

	/**
	 * Sets the element int the position (row, col) of the matrix to nbr.
	 * 
	 * @param row the row where the element will be inserted
	 * @param col the column where the element will be inserted
	 * @param nbr the value to insert into the matrix
	 */
	public void setSudoku(int row, int col, int nbr) {
		sudoku[row][col] = nbr;
	}

	/**
	 * This method solves the sudoku that is represented by the matrix sudoku.
	 * Returns true if the sudoku was solved, false otherwise.
	 * 
	 * @param sudoku the matrix that represents the sudoku
	 * @return returns true if the sudoku was solved, false otherwise
	 */
	public boolean solve(int[][] sudoku) {
		this.sudoku = sudoku;
		if (validSudoku(this.sudoku)) { // Checks if the sudoku is valid in the first place
			return solve(0, 0); // If it is, call the recursive method
		} else {
			return false;
		}
	}

	/*
	 * Recursive method to solve the sudoku.
	 */
	private boolean solve(int row, int col) {
		if (row != 9) { // If the row is 9 then the sudoku is solved and the method will return true
			if (sudoku[row][col] == 0) { // Checks if the slot is empty
				for (int nbr = 1; nbr <= 9; nbr++) { // Sequentially tries to insert 1 to 9
					if (checkNumber(row, col, nbr)) {
						setSudoku(row, col, nbr);
						if (col != 8) { // Solves for the next element
							if (solve(row, col + 1)) {
								return true;
							}
						} else {
							if (solve(row + 1, 0)) {
								return true;
							}
						}
					}
				}
				setSudoku(row, col, 0); // If it is not possible to solve for the element
				return false; // Set the element to 0 and return false
			} else {
				if (checkNumber(row, col, sudoku[row][col])) { // If the element has a value solve for the next element
					if (col != 8) {
						if (solve(row, col + 1)) {
							return true;
						}
					} else {
						if (solve(row + 1, 0)) {
							return true;
						}
					}
				}
			}
			return false;
		}
		return true;
	}

	/*
	 * Checks if the number can be inserted to the sudoku.
	 */
	private boolean checkNumber(int row, int col, int nbr) {
		int rowTL = 3 * (row / 3); // The coordinates for the top left of
		int colTL = 3 * (col / 3); // the 3x3 region the element is in

		for (int rr = rowTL; rr <= rowTL + 2; rr++) { // Checks if the region contains nbr
			for (int cc = colTL; cc <= colTL + 2; cc++) {
				if (rr != row && cc != col && sudoku[rr][cc] == nbr) {
					return false;
				}
			}
		}

		for (int ind = 0; ind <= 8; ind++) { // Checks if the row and column contains nbr
			if (sudoku[ind][col] == nbr && ind != row) {
				return false;
			} else if (sudoku[row][ind] == nbr && ind != col) {
				return false;
			}
		}
		return true;
	}

	/*
	 * Checks if the sudoku is valid.
	 */
	private boolean validSudoku(int[][] sudoku) {
		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 9; col++) {
				if ((sudoku[row][col] == 1 || sudoku[row][col] == 2 || sudoku[row][col] == 3 || sudoku[row][col] == 4
						|| sudoku[row][col] == 5 || sudoku[row][col] == 6 || sudoku[row][col] == 7
						|| sudoku[row][col] == 8 || sudoku[row][col] == 9)) {
					if (!(checkNumber(row, col, sudoku[row][col]))) {
						return false;
					}
				} else if (sudoku[row][col] != 0) {
					return false;
				}
			}
		}
		return true;

	}

	/**
	 * Clears the sudoku. Every element is set to 0.
	 */
	public void clear() {
		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 9; col++) {
				this.sudoku[row][col] = 0;
			}
		}
	}

}
